# Python TaskNote

Timing:
- [27.08.2021 19:22 (UTC+3)] Начало выполнение работы, ознакомление с ТЗ, инструментарием, БД.
- [27.08.2021 19:45 (UTC+3)] Сформирована основа БД, добавлена документация по БД.
- [27.08.2021 10:40 (UTC+3)] Попытка добавления многопоточной работы с БД, не нашел вариантов органичен функционалом Core. Добавление сартового объявления таблиц в БД.
- [27.08.2021 23:30 (UTC+3)] Проверка создания всех таблиц. Конец работы на первый день. (3,5 часа работы 30 мин на перерыв).
- [28.08.2021 14:35 (UTC+3)] Возобновление работы над сервером.
- [28.08.2021 16:35 (UTC+3)] Добавлена авторизация, абстракция ответа от БД, обработка ошибок, перерыв (30 мин на перерыв).
- [28.08.2021 18:40 (UTC+3)] Реализованы функции для взаимодействия с БД, протестирована работа с БД.
- [28.08.2021 20:05 (UTC+3)] Изучена базовая работа с Flask. Меняю рабочее место. (50 мин на перерыв).
- [28.08.2021 20:55 (UTC+3)] Возвращение к раборе, реализация базового API.
- [28.08.2021 23:30 (UTC+3)] Настроено взаимодействие между БД и API. Конец работы на второй день.
- [29.08.2021 13:00 (UTC+3)] Возобновление работы над сервером.
- [29.08.2021 17:35 (UTC+3)] Сделан основной функционал приложения, отсутствует статистика. (4 часа работы 1 час на перерыв)
- [29.08.2021 19:10 (UTC+3)] ТЗ выполнено в полном объеме (30 мин на перерыв)

# API

### **``/`` [ GET ]**
**Переадресовывает пользователя на регистрацию или на домашнюю страницу, при отсутсивии ключа сессии.**
HTML - форма отсутствует.

### **```/login``` [ GET, POST ]**
**Форма авторизации, переадресовывет пользователя на домашнюю страницу ```/home```, в случае присутствия ключа сессии.**
HTML - форма login.html

### **```/home``` [ GET, POST ]**
**Домашняя страница пользователя. На странице размещаются все новые задачи, которые были записаны в БД.   
В случае отсутствия ключа сессии переадресуется на страницу авторизации ```/login```**  
HTML - форма note.html

### **```/add-note``` [ GET, POST ]**
**Страница добавления задачи. Доступна как разегистрированным так и не зарегистрированным пользователям**  
HTML - форма add_note.html

### **```/my-note``` [ GET ]**
**Страница выбраных (выполняемых) пользователем задач.  
В случае отсутствия ключа сессии переадресуется на страницу авторизации ```/login```**   
HTML - форма my_note.html

### **```/set-note-ready``` [ POST ]**
**Метод реализующий изменение статуса задачи на статус "Закончено".  
В случае отсутствия ключа сессии переадресуется на страницу авторизации ```/login```**  
HTML - форма отсутствует

### **```/set-note-cancel``` [ POST ]**
**Метод реализующий изменение статуса задачи на статус "Отменено".  
В случае отсутствия ключа сессии переадресуется на страницу авторизации ```/login```**  
HTML - форма отсутствует

### **```/archive``` [ GET ]**
**Страница отмененых и законченых (закрытых) задач.  
В случае отсутствия ключа сессии переадресуется на страницу авторизации ```/login```**  
HTML - форма archive.html

### **```/statistics```[ GET ]**
**Страница статистики по задачам.  
В случае отсутствия ключа сессии переадресуется на страницу авторизации ```/login```**  
HTML - форма statistics.html

### **```/logout``` [ GET ]**
**Страница онуления сессии. Переадресуется на страницу авторизации ```/login```**
HTML - форма отсутствует

# Database tables

###  **```users```** - таблица пользователей, хранит основные данные о пользователе

| Поле | Тип Данных | Описание |
|------|------------|----------|
| id   | INTEGER (AUTO) | Индентификатор строки |
| email|  TEXT          | Логин пользователя для авторизации в системе |
| pwd | TEXT            | Пароль пользователя для авторизации в системе |
| firstname | TEXT      | Имя пользователя |
| lastname | TEXT       | Фамилия пользователя |
| middlename | TEXT     | Отчество |


###  **```status_note```** - таблица статусов задач 

| Поле | Тип Данных | Описание |
|------|------------|----------|
| id   | INTEGER (AUTO) | Индентификатор строки |
| name |  TEXT          | Наименование статуса  |


###  **```task_note```** - таблица задач 

| Поле | Тип Данных | Описание |
|------|------------|----------|
| id   | INTEGER (AUTO)             | Индентификатор строки |
| user |  INTEGER                      | Пользователь ответственный за выполнение задания |
| date_create | INTEGER             | Дата создания (UNIX) |
| date_start | INTERGER             | Дата начала (UNIX) |
| date_update | INTEGER             | Дата обновления (UNIX) |
| title | TEXT                      | Наименование задачи |
| src | TEXT                      | Описание задачи |
| status | INTEGER (status_note.id) | Статус задачи |

###  **```token_session```** - таблица токенов сессии 

| Поле | Тип Данных | Описание |
|------|------------|----------|
| id   | INTEGER (AUTO) | Индентификатор строки |
| user | INTEGER (users.id) | Наименование статуса |
| token | TEXT | Токен сессии |
| date_update | INTEGER |  Дата обновления (UNIX) |

# Основные сущности реализованные в моделях

### **Class ```Error```**
**Реализовывет абстракцию ошибки представленом в болеее удобном виде для обработки.**  
см. models/responce.py
    
    class Error:
        error = str

        def __init__(self, error):
            self.error = error

### **Class ```Responce```**
**Реализовывет абстракцию ответа от базы данных представленный в более удобном виде для работы с ответами из БД.**  
см. models/responce.py

    class Responce:
        Data = None
        Error = None

        def __init__(self, T):
            if isinstance(T, Error):
                self.Error = T
            else:
                self.Data = T
        
        def error(self) -> str:
            return self.Error.error

### **Class ```Note```**
**Реализовывает сущности задачи.**  
см. models/note.py

    class Note:
        ID = int
        User = str
        DateCreate = str
        DateStart = str
        DateUpdate = str
        Title = str
        Value = str
        Status = str

### **Class ```StatisticNote```**
**Реализовывет сущность статистики по БД.**  
см. models/statistics.py

    class StatisticNote:
        NewNote = int
        ActiveNote = int
        ReadyNote = int
        CancelNote = int
        TimingWork = int


Пользователь не был ввыведен в сущность по причине отсутствие особого функционала по взаимодействию с его данными и параметрами, но при добавление фультрации задачь по пользователям, была бы реализована подобная сущность. Стоит отсутить что её не хватает и при подобной реализации)