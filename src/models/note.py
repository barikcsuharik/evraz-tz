class Note:
    ID = int
    User = str
    DateCreate = str
    DateStart = str
    DateUpdate = str
    Title = str
    Value = str
    Status = str
    
    def __init__(self, ID, User, DateCreate, DateStart, DateUpdate, Title, Value, Status):
        self.ID = ID
        self.User = User
        self.DateCreate = DateCreate
        self.DateStart = DateStart
        self.DateUpdate = DateUpdate
        self.Title = Title
        self.Value = Value
        self.Status = Status
